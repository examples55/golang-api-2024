package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type User struct {
	Email string `json:"email"`
}

func getUserHandler(ctx context.Context, event events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Printf("getUserHandler called with event: %+v", event)

	response := events.APIGatewayProxyResponse{
		StatusCode:        204,
		Headers:           nil,
		MultiValueHeaders: nil,
		Body:              "",
		IsBase64Encoded:   false,
	}

	user := User{Email: "user@example.com"}

	userJson, err := json.Marshal(user)

	if err != nil {
		fmt.Printf("failed marshalling user struct: %s", err.Error())
		response.StatusCode = 500
	} else {
		response.Body = string(userJson)
		response.StatusCode = 200
	}

	return response, nil // err is nil so that the 500 error returns to the user.
}

func main() {
	lambda.Start(getUserHandler)
}
